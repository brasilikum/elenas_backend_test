from elenas_task.models import Task
from elenas_task.pagination import PostPageNumberPagination
from elenas_task.serializers import TaskSerializer
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
# Create your views here.


class TaskViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    pagination_class = PostPageNumberPagination
