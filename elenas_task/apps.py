from django.apps import AppConfig


class ElenasTaskConfig(AppConfig):
    name = 'elenas_task'
